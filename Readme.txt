tutorial: https://www.youtube.com/watch?v=ZVnjOPwW4ZA

vscode extensions:
ES7+ React/Redux/React-Native
JavaScript and TypeScript Nightly
Tailwind CSS IntelliSense

npx create-next-app@latest
npx create-next-app@13.4 

prompts:
--
TypeScript Yes
ESLint Yes
Tailwind CSS Yes
src/ directory No
App Router Yes
customize default import alias No

cd next-app
npm run dev

hott://localhost:3000

project structure:
------
app folder contains file based routes
layout.tsx --- basic react component, common layout for our pages
page.tsx --- home page


In the new App Router we are using:
All routes will be with name page.tsx but in different folders.
app\<foldername>\page.tsx --> http://localhost:3000/<foldername>

app\users\page.tsx  --> http://localhost:3000/users

In pages router, all files in the folder are publicly accessible, but in new router only page.tsx is accessible.
Also pages router doesn't support server components.

Same with subfolders also.

app\users\new\page.tsx  --> http://localhost:3000/users/new

Client side and service side rendering:
-----------
server components cannot:
-listen to brower events
-access browser APIs
- maintain state
- use effects


All are server components by default.
Which required client interaction, we will set that as client component.

fetch data on client:
- useState() + useEffect()
- React query

Disadvantages for client data fetching:
Large bundles 
Resource intensive 
No SEO 
Less secure 
Extra roundtrip to server 

dummy api data for testing:
https://jsonplaceholder.typicode.com 

Caching the data by fetch function in file system.
Caching works for fetch function only not for thridpaty ones like axios.

Static Rendering(Build time):
- Render at build time. Static data like html rendered once during build time in cache and gets cached data at request time.
- Dynamic rendering happens at request time. 
- Differetentiate at (npm run build) with lambda(dynmic) and circle(static) in front of routes.


tailwind css:
Paddings 
p-[number] 
px-[number] 
py- [number] 
pt-[number] 
pr- [number] 
pb-[number] 
pl-[number] 

Margins 
m- [number] 
mx-[number] 
my - [number] 
mt-[number] 
mr- [number] 
mb-[number] 
ml - [number] 

Text:

Size 
text-xs 
font-thin 
text-sm 
font-light 
text-base 
text-lg 
text-xl 
font-bold 
text-2x1 
text-3x1 


Color 
text- [color] 
bg- [color] 

Thickness 
font-normal 
font-medium 
font-light 
font-thin 
font-bold

-----------
daisyUI -- daisyui.com/docs/install/
- bootstrap for tailwind.

button -- daisyui.com/components/button/

themes -- daisyui.com/docs/themes/
