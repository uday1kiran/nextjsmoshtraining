//rafce
import React from "react";

interface User {
  id: number;
  name: string;
  email: string;
}

const UsersPage = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/users", {
    cache: "no-store",
  }); //instead of cache disable option above, {next: {revalidate: 10 }} --> to fetch data for every 10 seconds.
  const users: User[] = await res.json();

  return (
    <>
      <h1>Users</h1>
      <p>{new Date().toLocaleTimeString()}</p>
      {/* during dev(npm run dev) it changes data but
      in prod(npm run build ; npm start) it go static redenderingg and uses same data if caching enabled.
  */}
      <table className="table table-bordered">
        {/* thead>tr>th*2 */}
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id}>
              <td>{user.name}</td>
              <td>{user.email}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default UsersPage;
