import Image from "next/image";
import Link from "next/link";
import ProductCard from "./components/ProductCard";

export default function Home() {
  return (
    <main>
      <h1>
        Hello World - after cleanup of this page and remvoed linear gradient
        style
        <a href="/users">
          Users - This will download all pages again when you click.
        </a>
        <br />
        <Link href="/users">
          Users - This will download only target page when you click, using
          client side navigation.
        </Link>
        <ProductCard />
      </h1>
    </main>
  );
}
