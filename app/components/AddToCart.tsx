"use client"; //to make it client side rendering component and this file dependencies also included to bundle
import React from "react";

const AddToCart = () => {
  return (
    <div>
      <button className="btn btn-primary" onClick={() => console.log("Click")}>
        Add to Cart won't work in server side
      </button>
    </div>
  );
};

export default AddToCart;
